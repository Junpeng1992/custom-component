# gwt-custom-component

> A Vue.js custom project
> Form Builder

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).


## doc

https://gitee.com/Junpeng1992/custom-component/wikis/%E6%95%B4%E4%BD%93%E9%85%8D%E7%BD%AE%E4%BB%A3%E7%A0%81?sort_id=3415320

### 表单生成器示例
```vue



<template>
  <div id="app">
    <createForm :config-data="configData" @getFormData="getFormData" />

    <div>
      初始值：
      <ul>
        <li v-for="(li,i) in configData.list" :key="i">
          {{ li.config.type }}-{{ li.config.fileName }}-value:{{ li.data.value }}
        </li>
      </ul>
      返回值：
      <pre>
        {{ formData }}
      </pre>

    </div>

  </div>
</template>

<script>
// import createForm from './components/createForm'
export default {
  name: 'App',
  // components: { createForm },
  data() {
    return {
      formData: [],
      configData: {
        'list': [
          {
            config: {},
            data: {}
          }
        ],
        'config': {
          'rows': 1, // 设置列数
          'labelWidth': 100,
          'labelPosition': 'left',
          'size': 'small',
          'customClass': '',
          'ui': 'element',
          'layout': 'horizontal',
          'labelCol': 3,
          'width': '100%',
          'hideLabel': false,
          'hideErrorMessage': false
        }
      }

    }
  },
  created() {
    this.getData()
  },
  methods: {
    getFormData(data) {
      console.log(data)
      this.formData = data
    },
    getData() {
      const config = [
        { 'type': 'input', 'name': '姓名', 'fileName': 'name' },
        { 'type': 'radio', 'name': '年龄', 'fileName': 'age',
          'options': [
            { 'value': '10岁', 'label': '1' }, { 'value': '20岁', 'label': '2' }
          ]
        },
        { 'type': 'checkBox', 'name': '技能', 'fileName': 'skill', 'options': ['设计', '前端', 'C#', 'JAVA'] },
        { 'type': 'rate', 'name': '自我评分', 'fileName': 'score' },
        { 'type': 'input', 'name': '自我评价', 'fileName': 'evaluate' },
        { 'type': 'inputNumber', 'name': '衣服尺码', 'fileName': 'size' },
        { 'type': 'select', 'name': '出生地', 'fileName': 'born', 'options': [{ 'value': '1', 'label': '济南' }, { 'value': '2', 'label': '德州' }] },
        { 'type': 'switch', 'name': '婚否', 'fileName': 'marital', 'options': [
          { 'value': '0', 'color': '#13ce66', 'label': '未婚' },
          { 'value': '1', 'color': '#ff4949', 'label': '已婚' }
        ] }
      ]
      const data = [
        { 'value': '' },
        { 'value': 0 },
        { 'value': [] },
        { 'value': 0 },
        { 'value': '' },
        { 'value': '1' },
        { 'value': '1' },
        { 'value': '0' }
      ]

      const that = this
      that.configData.list = []

      for (let i = 0; i < config.length; i++) {
        const list = { config: [], data: [] }
        list.config = config[i]
        list.data = data[i]
        that.configData.list.push(list)
      }

      console.log(that.configData.list)
    }
  }
}

</script>

<style lang="scss">

</style>

```
