## 描述

左侧展开收起包裹组件


## 使用
把要折叠的组件包裹在`template`里面就可以了


## 示例

```
 <leftBox>
    <template>
        <tree/>// 要包裹的组件
    </template>
</leftBox>

```