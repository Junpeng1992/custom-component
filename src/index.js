import Vue from 'vue'
import App from './App.vue'

import moment from 'moment'// 导入文件
Vue.prototype.$moment = moment// 赋值使用
moment.locale('zh-cn')// 需要汉化

import echarts from 'echarts'
Vue.prototype.$echarts = echarts

import directives from './directives'
Vue.use(directives)

// 容器类组件
import Fold from './components/Fold/index'
import borderBox1 from './components/borderBox1/index'

// import Brands from './components/Brands/index'
import TimeLine from './components/TimeLine/index'

// 图表
import Gchart from './components/Gchart/index'

// 表单
import Gcheckbox from './components/Gcheckbox/index'
import Ginput from './components/Ginput/index'
import GinputNumber from './components/GinputNumber/index'
import Gradio from './components/Gradio/index'
import Grate from './components/Grate/index'
import Gselect from './components/Gselect/index'
import Gswitch from './components/Gswitch/index'
import Gtextarea from './components/Gtextarea/index'
import GdropdownTree from './components/GdropdownTree/index'

// 表格 Gtable
import Gtable from './components/Gtable/index'

// 时间选择
import GtimePoint from './components/GtimePoint/index'
import GtimeRange from './components/GtimeRange/index'

// 表单生成器
import createForm from './components/createForm/index'

export default function(Vue) {
  // Vue.use(Brands)
  Vue.use(TimeLine)
  Vue.use(Fold)
  Vue.use(Gchart)
  Vue.use(borderBox1)

  Vue.use(Gcheckbox)
  Vue.use(Ginput)
  Vue.use(GinputNumber)
  Vue.use(Gradio)
  Vue.use(Grate)
  Vue.use(Gselect)
  Vue.use(Gswitch)
  Vue.use(Gtextarea)
  Vue.use(GdropdownTree)
  Vue.use(createForm)

  Vue.use(Gtable)

  Vue.use(GtimePoint)
  Vue.use(GtimeRange)
}
