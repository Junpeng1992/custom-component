import moment from 'moment'

export default {
  transformTozTreeFormat: function(sNodes, idName = 'id', pidName = 'pid') {
    var i, l
    var r = []
    var tmpMap = {}
    for (i = 0, l = sNodes.length; i < l; i++) {
      tmpMap[sNodes[i][idName]] = sNodes[i]
      sNodes[i].id = sNodes[i][idName]
    }
    for (i = 0, l = sNodes.length; i < l; i++) {
      var p = tmpMap[sNodes[i][pidName]]
      if (p && sNodes[i][idName] !== sNodes[i][pidName]) {
        var children = this.nodeChildren(p)
        if (!children) {
          children = this.nodeChildren(p, [])
        }
        children.push(sNodes[i])
      } else {
        r.push(sNodes[i])
      }
    }
    return r
  },
  searchTree(data, keyName, key) {
    const result = []
    function search(list, id) {
      list.map((item) => {
        if (item[keyName] === id) {
          result.push(item)
        } else {
          if (item.children && item.children.length) {
            return search(item.children, id)
          }
        }
      })
    }
    search(data, key)
    return result[0]
  },
  searchTreeAll(data, keyName, key, onlyFirst = true) {
    const result = []
    let isFind = false
    function search(list, id) {
      if (onlyFirst && isFind) {
        return
      }
      for (var item of list) {
        if (item[keyName] === id) {
          result.push(item)
          isFind = true
        }
        if (item.children && item.children.length) {
          search(item.children, id)
        }
      }
    }
    search(data, key)
    return result
  },
  // flatTree: function flatTree(tree, keyName) {
  //   var result = []
  //   // 生成该结构的函数，希望可以帮到你
  //   function formatTree(arr, parentId) {
  //     arr.forEach(item => {
  //       result.push({ parentId: parentId, ...item })
  //       if (item.children && item.children.length) {
  //         formatTree(item.children, item[keyName])
  //       }
  //     })
  //   }
  //   formatTree(tree)

  //   return result
  // },
  searchParentsFromTree(tree, keyName, key) {
    var list = flatTree(tree, keyName)
    var result = []
    function searchParent(key) {
      var current = list.filter(item => item[keyName] === key)[0]
      result.push(current)
      if (current.parentId) {
        searchParent(current.parentId)
      }
    }
    searchParent(key)
    return result
  },

  nodeChildren: function(node, newChildren) {
    if (typeof newChildren !== 'undefined') {
      node.children = newChildren
    }
    return node.children
  },
  // format 周几
  formatWeek(weekNum) {
    let date = ''
    switch (weekNum) {
      case 0: date = '周日'
        break
      case 1: date = '周一'
        break
      case 2: date = '周二'
        break
      case 3: date = '周三'
        break
      case 4: date = '周四'
        break
      case 5: date = '周五'
        break
      case 6: date = '周六'
        break
    }

    return date
  },
  textMap: {
    create: '新增',
    update: '编辑',
    view: '查看',
    delete: '删除',
    query: '查询'
  },
  dialogStatus: {
    create: 'create',
    update: 'update',
    view: 'view'
  },
  maxExcelRowCount: 1048576,
  WDictData: {
    Station: {
      StationConfigType: [
        { code: 0, name: '默认' }
      ],
      StationAttr: [
        { code: 1, name: '新建' },
        { code: 2, name: '填平补齐' },
        { code: 3, name: '上收已建' },
        { code: 99, name: '其他' }
      ],
      StationCategory: [
        { code: 1, name: '水站' }
      ],
      StationType: [
        { code: 1, name: '河流' },
        { code: 2, name: '湖库' },
        { code: 3, name: '地下水' }
      ],
      StationType2: [
        { code: 1, name: '河流' },
        { code: 2, name: '湖库' }
      ],
      StationPositonAttr: [
        { code: 1, name: '点位' },
        { code: 2, name: '断面' }
      ],
      qualitativevalue: [
        { code: '100', name: '-' },
        { code: '101', name: '+' },
        { code: '102', name: '++' },
        { code: '103', name: '+++' }
      ],
      biotype: [
        { code: 'algae', name: '藻类生物' },
        { code: 'Benthic', name: '底栖生物' }
      ],
      datastate: [
        { code: 0, name: '已录入' },
        { code: 1, name: '已上报' },
        { code: 2, name: '已审核' },
        { code: 3, name: '已驳回' }
      ],
      StationControlLevel: [
        { code: 1, name: '国控考核' },
        { code: 2, name: '省控' },
        { code: 3, name: '市控' },
        { code: 4, name: '县控' },
        { code: 5, name: '国控趋势科研' }
      ],
      StationAreaLevelDesc: [
        { code: 1, name: '国家' },
        { code: 2, name: '省级' },
        { code: 3, name: '地级' },
        { code: 4, name: '县级' }
      ],
      StationExitAndEntry: [
        { code: 1, name: '请选择' },
        { code: 2, name: '出境' },
        { code: 3, name: '入境' }
      ],
      StationStatus: [
        { code: 0, name: '停用' },
        { code: 1, name: '启用' },
        { code: 2, name: '撤销' }

      ],
      calculateType: [
        { code: 0, name: '按断面计算' },
        { code: 1, name: '按湖库计算' }
      ],
      dataResource: [
        { code: 1, name: '地表水' },
        { code: 2, name: '入海河流' },
        { code: 3, name: '近岸海域' },
        { code: 4, name: '饮用水源地' },
        { code: 5, name: '地下水' },
        { code: 6, name: '应急监测' },
        { code: 7, name: '水生生物' }
      ],
      RunMode: [
        { code: 1, name: '采测分离' },
        { code: 2, name: '自动监测' }
      ],
      WaterQualityLevel: [
        { code: 1, name: 'Ⅰ类' },
        { code: 2, name: 'Ⅱ类' },
        { code: 3, name: 'Ⅲ类' },
        { code: 4, name: 'Ⅳ类' },
        { code: 5, name: 'Ⅴ类' },
        { code: 6, name: '劣Ⅴ类' }
      ],
      WaterSourceType: [
        { code: 1, name: '地表水' },
        { code: 2, name: '集中式饮用水' },
        { code: 3, name: '地下水' }
      ],
      Branches: [
        { code: 0, name: '请选择' },
        { code: 1, name: '干流' },
        { code: 2, name: '一级支流' },
        { code: 3, name: '二级支流' },
        { code: 4, name: '三级支流' },
        { code: 5, name: '四级支流' },
        { code: 6, name: '出入湖' },
        { code: 7, name: '湖泊' },
        { code: 8, name: '水库' }
      ],

      FunctionType: [
        { code: 0, name: '请选择' },
        { code: 1, name: 'Ⅰ类' },
        { code: 2, name: 'Ⅱ类' },
        { code: 3, name: 'Ⅲ类' },
        { code: 4, name: 'Ⅳ类' },
        { code: 5, name: 'Ⅴ类' },
        { code: 6, name: '劣Ⅴ类' }
      ],
      BoundaryType: [
        { code: 0, name: '请选择' },
        { code: 1, name: '省界' },
        { code: 2, name: '市界' }
      ],
      WaterQualityAnalysis: [
        { code: 0, name: '请选择' },
        { code: 1, name: '是' },
        { code: 2, name: '否' }
      ],
      JointMonitoring: [
        { code: 1, name: '是' },
        { code: 2, name: '否' }
      ]
    },
    MonitorFactor: {
      WaterQualityType: [
        { code: 0, name: '通用' },
        { code: 1, name: '河流' },
        { code: 2, name: '湖库' }

      ],
      MFDataRevisionType: [
        { code: 0, name: '默认' }
      ],
      MFQualityType: [
        { code: 1, name: 'I类' },
        { code: 2, name: 'II类' },
        { code: 3, name: 'III类' },
        { code: 4, name: 'IV类' },
        { code: 5, name: 'V类' },
        { code: 6, name: '劣Ⅴ类' }
      ],
      MonitorType: [
        { code: '1', name: '物理和综合指标' },
        { code: '2', name: '生物指标' },
        { code: '3', name: '放射性指标' },
        { code: '4', name: '其他指标' },
        { code: '5', name: '金属及金属化合物' },
        { code: '6', name: '无机污染物' },
        { code: '7', name: '油类' },
        { code: '8', name: '酚' },
        { code: '9', name: '脂肪烃和卤代脂肪烃' },
        { code: '10', name: '芳香族化合物' },
        { code: '11', name: '胺' },
        { code: '12', name: '多氯联苯' },
        { code: '13', name: '醚' },
        { code: '14', name: '酯' },
        { code: '15', name: '醇' },
        { code: '16', name: '醛' },
        { code: '17', name: '有机酸' },
        { code: '18', name: '农药' },
        { code: '19', name: '消毒剂及其副产物' },
        { code: '20', name: '其他有机物' },
        { code: '21', name: '大气指标' }
      ],
      FactorSimpleName: [
        { code: '1', name: 'pH' },
        { code: '2', name: 'DO' },
        { code: '3', name: 'CODMn' },
        { code: '4', name: 'CODcr' },
        { code: '5', name: 'BOD5' },
        { code: '6', name: 'NH3-N' },
        { code: '7', name: 'TP' },
        { code: '8', name: 'TN' },
        { code: '9', name: 'Cu' },
        { code: '10', name: 'Zn' },
        { code: '11', name: 'F-' },
        { code: '12', name: 'Se' },
        { code: '13', name: 'As' },
        { code: '14', name: 'Hg' },
        { code: '15', name: 'Cd' },
        { code: '16', name: 'Cr6+' },
        { code: '17', name: 'Pb' },
        { code: '18', name: 'CN-' },
        { code: '19', name: 'LAS' },
        { code: '20', name: 'Chla' },
        { code: '21', name: 'NO3-' },
        { code: '22', name: 'NO2-' },
        { code: '23', name: 'EV' },
        { code: '24', name: 'AT' },
        { code: '25', name: 'AH' },
        { code: '26', name: 'ENP' },
        { code: '27', name: 'EXP' }
      ],
      Unit: [
        { code: '1', name: '无量纲' },
        { code: '2', name: 'NTU' },
        { code: '3', name: '℃' },
        { code: '4', name: 'mg/L' },
        { code: '5', name: 'μg/L' },
        { code: '6', name: 'ng/L' },
        { code: '7', name: '个/L' },
        { code: '8', name: 'L/s' },
        { code: '9', name: 'μS/cm' },
        { code: '10', name: 'mg/m³' },
        { code: '11', name: 'm³' },
        { code: '12', name: 'kg' },
        { code: '13', name: 'g' },
        { code: '14', name: 'mg' },
        { code: '15', name: 'cells/mL' },
        { code: '16', name: '%' },
        { code: '17', name: 'V' },
        { code: '18', name: 'MPa' },
        { code: '19', name: 'kPa' },
        { code: '20', name: '度' },
        { code: '21', name: 'm/s' },
        { code: '22', name: 'TU' },
        { code: '23', name: '个/升' },
        { code: '24', name: 'M' }
      ]
    },
    StandardProtocol: {
      StandardType: [
        { code: '1', name: '国家标准' },
        { code: '2', name: '地方标准' }
      ]
    },
    DTSCommand: {
      DTSCommandCategory: [
        { code: '1', name: '初始化命令' },
        { code: '2', name: '参数命令' },
        { code: '3', name: '数据命令' },
        { code: '4', name: '控制命令' },
        { code: '5', name: '交互命令' }
      ],
      DTSCommandType: [
        { code: '1', name: '请求命令' },
        { code: '2', name: '上传命令' },
        { code: '3', name: '通知命令' }
      ]
    },
    DTSCParameter: {
      DTSCParameterType: [
        { code: 1, name: '枚举' },
        { code: 2, name: '文本' },
        { code: 3, name: '时间' }
      ]
    },
    DTSOnSite: {
      EquipmentCategory: [
        { code: '1', name: '在线监控（监测）仪器仪表' },
        { code: '2', name: '数采仪' },
        { code: '3', name: '辅助设备' },
        { code: '4', name: '质控仪' }
      ],
      InformationCategory: [
        { code: '1', name: '日志' },
        { code: '2', name: '状态' },
        { code: '3', name: '参数' }
      ]
    },
    Log: {
      LogType: [
        { code: 1, name: '新增' },
        { code: 2, name: '修改' }
      ],
      LogStatus: [
        { code: 1, name: '正常' },
        { code: 2, name: '删除' }
      ]
    },
    AfterSaleManage:
        {
          FaultType: [
            { code: 1, name: '硬件故障' },
            { code: 2, name: '软件故障' },
            { code: 3, name: '突发事件' }
          ],
          HardwareReason: [
            { code: 1, name: '工控机故障' },
            { code: 2, name: 'VPN故障' },
            { code: 3, name: '仪器故障' },
            { code: 8, name: '其他' }
          ],
          SoftwareReason: [
            { code: 4, name: '传输故障' },
            { code: 5, name: '数采故障' },
            { code: 8, name: '其他' }
          ],
          Emergency: [
            { code: 6, name: '停电' },
            { code: 7, name: '水毁' },
            { code: 8, name: '其他' }
          ],
          StemFrom: [
            { code: 1, name: '远程巡视' },
            { code: 2, name: '上站巡查' },
            { code: 3, name: '客户反馈' },
            { code: 4, name: '告警提醒' }
          ],
          Status: [
            { code: 1, name: '待处理' },
            { code: 2, name: '待确认' },
            { code: 3, name: '已结束' }
          ]
        },
    StationEquipment:
        {
          OperateType:
                [
                  { code: 0, name: '空闲' },
                  { code: 2, name: '报废' }
                ]
        },
    Equipment:
        {
          EquipmentType:
                [
                  { code: 1, name: '在线监控（监测）仪器仪表' },
                  { code: 2, name: '数据采集传输仪' },
                  { code: 3, name: '辅助设备' },
                  { code: 4, name: '质控仪' }
                ],
          AnalyseMethod:
                [
                  { code: 1, name: '高锰酸钾氧化-ORP电位滴定法' },
                  { code: 2, name: '热敏电阻法' },
                  { code: 3, name: '电解池法' },
                  { code: 4, name: '高锰酸钾氧化法' },
                  { code: 5, name: '荧光法' },
                  { code: 6, name: '玻璃电极法' },
                  { code: 7, name: '钼酸铵分光光度法' },
                  { code: 8, name: '光散射法' },
                  { code: 9, name: '90度散射法' },
                  { code: 10, name: '碱性过硫酸钾分光光度法' },
                  { code: 11, name: '紫外分光光度法' },
                  { code: 12, name: '电极法' },
                  { code: 13, name: '温度传感器法（热电阻）' },
                  { code: 14, name: '分光光度法' },
                  { code: 15, name: '碱性过硫酸钾消解紫外分光光度法' },
                  { code: 16, name: '热电阻法' },
                  { code: 17, name: '电极滴定法' },
                  { code: 18, name: '水杨酸分光光度法' }
                ],
          Reagent:
                [
                  { code: 1, name: '菌种' },
                  { code: 2, name: '盐水' },
                  { code: 3, name: '标样' },
                  { code: 4, name: '蒸馏水' },
                  { code: 5, name: '硫酸汞' },
                  { code: 6, name: '重铬酸钾溶液' },
                  { code: 7, name: '硫酸' },
                  { code: 8, name: '草酸钠溶液' },
                  { code: 9, name: '零点标准溶液' },
                  { code: 10, name: '标准溶液' },
                  { code: 12, name: '高锰酸钾溶液' },
                  { code: 13, name: '无试剂' },
                  { code: 14, name: '纳氏试剂' },
                  { code: 15, name: '还原剂' },
                  { code: 16, name: '碱溶液' },
                  { code: 17, name: '酸溶液' },
                  { code: 18, name: '福尔马肼' },
                  { code: 19, name: '电导率标准溶液' },
                  { code: 20, name: '氨氮标准溶液' },
                  { code: 21, name: '校准液' },
                  { code: 22, name: '次氯酸钠溶液' },
                  { code: 23, name: '稀硫酸' },
                  { code: 24, name: '氢氧化钠溶液' },
                  { code: 25, name: '稀盐酸' },
                  { code: 26, name: '亚硝基亚铁氰化钠' },
                  { code: 27, name: '二氯异氰尿酸钠' },
                  { code: 28, name: '二水合柠檬酸三钠' },
                  { code: 29, name: '葡萄糖' },
                  { code: 30, name: '酒石酸锑钾' },
                  { code: 31, name: '磷酸二氢钾' },
                  { code: 32, name: '清洗剂' },
                  { code: 33, name: 'pH标准溶液' },
                  { code: 34, name: '叶绿素标准溶液' },
                  { code: 35, name: '总氮标准溶液' },
                  { code: 36, name: '总磷标准溶液' },
                  { code: 37, name: '滴定剂' },
                  { code: 38, name: '催化剂' },
                  { code: 39, name: '水杨酸钠溶液' },
                  { code: 40, name: '过硫酸钠溶液' },
                  { code: 41, name: '钼酸盐溶液' },
                  { code: 42, name: '抗坏血酸溶液' },
                  { code: 43, name: '过硫酸钾溶液' },
                  { code: 44, name: '氯化铵' },
                  { code: 45, name: '乙二胺四乙酸二钠，二水' },
                  { code: 46, name: '钼酸铵，四水' },
                  { code: 47, name: '乙酸' },
                  { code: 48, name: '亚硝基铁氰化钠' },
                  { code: 49, name: '钼酸铵' },
                  { code: 50, name: '中和液' },
                  { code: 51, name: '电解液' },
                  { code: 52, name: '消解液' },
                  { code: 53, name: '镀膜液' },
                  { code: 54, name: '清洗液' },
                  { code: 55, name: '缓冲液' },
                  { code: 56, name: '还原液' },
                  { code: 57, name: '显色剂' },
                  { code: 58, name: '调节液' },
                  { code: 59, name: '氧化剂' },
                  { code: 60, name: '中和液' },
                  { code: 61, name: '萃取剂' },
                  { code: 62, name: '中和液（碱性法专用）' },
                  { code: 63, name: '吸收液' },
                  { code: 64, name: '释放剂' },
                  { code: 65, name: '掩蔽剂' },
                  { code: 66, name: '缓冲溶液' },
                  { code: 67, name: '盐酸' },
                  { code: 68, name: '酒石酸钾钠' }
                ],
          OperateType:
                [
                  { code: 0, name: '空闲' },
                  { code: 1, name: '绑定' },
                  { code: 2, name: '报废' }
                ],
          IsUse:
                [
                  { code: 1, name: '启用' },
                  { code: 0, name: '停用' }
                ],
          IsHeavymetal:
                 [
                   { code: 1, name: '是' },
                   { code: 0, name: '否' }
                 ],
          WarningType:
                 [
                   { code: 1, name: '异常预警' },
                   { code: 0, name: '每日异常' }
                 ],
          DailyAnomaly:
                 [
                   { code: 0, name: '自动科' },
                   { code: 1, name: '运维人员' },
                   { code: 2, name: '全部人员' }
                 ],
          AbnormalEarlyWarning:
                 [
                   { code: 0, name: '第一次市级预警' },
                   { code: 1, name: '第二次市级预警' },
                   { code: 2, name: '第三次市级预警' },
                   { code: 3, name: '第四次市级预警' },
                   { code: 4, name: '第一次省级级预警' },
                   { code: 5, name: '第二次省级级预警' },
                   { code: 6, name: '第三次省级级预警' },
                   { code: 7, name: '第四次省级级预警' },
                   { code: 8, name: '第一次省级应急' }
                 ]

        },
    QualityControlPlan: {
      QualityControlPlanType: [
        { code: 1, name: '标样核查' },
        { code: 2, name: '平行样测试' },
        { code: 3, name: '加标回收' },
        { code: 4, name: '零点核查' },
        { code: 5, name: '跨度核查' }
      ],
      QualityControlPlanDay: [
        { code: 1, name: '日' },
        { code: 2, name: '周' },
        { code: 3, name: '月' }
      ]
    },
    RecoveryRate:
        {
          RecoveryFactors:
                [
                  { code: 'GWTW0101019', name: '高锰酸盐指数' },
                  { code: 'GWTW0121003', name: '氨氮' },
                  { code: 'GWTW0121011', name: '总磷' },
                  { code: 'GWTW0121001', name: '总氮' }
                ]
        },
    KnowledgeBaseType:
        {
          KnowledgeType:
                [
                  { code: 1, name: '国家重点规划与计划' },
                  { code: 2, name: '风险评价准则与规范' },
                  { code: 3, name: '水质风险评价方法' },
                  { code: 4, name: '相关名词解释' }
                ]
        },
    LowerComputerInfoType:
        {
          InfoType:
                [
                  { code: 1, name: '日志' },
                  { code: 2, name: '状态' },
                  { code: 3, name: '参数' }
                ]
        },
    LogType: [
      { code: 0, name: '系统日志' },
      { code: 1, name: '仪器日志' },
      { code: 2, name: '用户日志' }
    ],
    FactorCode: [{
      pH: 'GWTW0101001', // ph
      DO: 'GWTW0101009', // 溶解氧
      T: 'GWTW0101010', // 水温
      Turbidity: 'GWTW0101003',
      EC: 'GWTW0101014'
    }],
    Basin: {
      BasinType: [
        { code: 0, name: '请选择类型' },
        { code: 1, name: '流域' },
        { code: 2, name: '水系' },
        { code: 3, name: '河流' },
        { code: 4, name: '湖泊' }
      ]
    },
    AlarmManager: {
      Category: [
        { code: 1025, name: '低于标准下限' },
        { code: 1026, name: '高于标准上限' },
        { code: 1029, name: '低于统计下限' },
        { code: 1030, name: '高于统计上限' },
        { code: 1001, name: '动环预警（烟感、水浸、断点、温湿度、电压）' },
        { code: 1006, name: '数据比对偏差' },
        { code: 1033, name: '数据正向突变' },
        { code: 1034, name: '数据负向突变' },
        { code: 1011, name: '持续上升趋势' },
        { code: 1031, name: '持续下降趋势' },
        { code: 1032, name: '水质变化幅度过大' },
        { code: 1027, name: '低于测量范围下限' },
        { code: 1028, name: '高于测量范围上限' },
        { code: 1013, name: '恒值' },
        { code: 1035, name: '稀有数据组合' },
        { code: 1036, name: '趋于稀有数据组合' },
        { code: 1037, name: '高频监测数据中心偏移' },
        { code: 1012, name: '零值' },
        { code: 1000, name: '数据标识异常' },
        { code: 1005, name: '标液核查' }
      ]
    },
    DateDict: {
      WeekDict: [
        { code: 1, name: '星期一' },
        { code: 2, name: '星期二' },
        { code: 3, name: '星期三' },
        { code: 4, name: '星期四' },
        { code: 5, name: '星期五' },
        { code: 6, name: '星期六' },
        { code: 7, name: '星期日' },
      ],
      Hour24Dict: [
        { code: 0, name: '0点' },
        { code: 1, name: '1点' },
        { code: 2, name: '2点' },
        { code: 3, name: '3点' },
        { code: 4, name: '4点' },
        { code: 5, name: '5点' },
        { code: 6, name: '6点' },
        { code: 7, name: '7点' },
        { code: 8, name: '8点' },
        { code: 9, name: '9点' },
        { code: 10, name: '10点' },
        { code: 11, name: '11点' },
        { code: 12, name: '12点' },
        { code: 13, name: '13点' },
        { code: 14, name: '14点' },
        { code: 15, name: '15点' },
        { code: 16, name: '16点' },
        { code: 17, name: '17点' },
        { code: 18, name: '18点' },
        { code: 19, name: '19点' },
        { code: 20, name: '20点' },
        { code: 21, name: '21点' },
        { code: 22, name: '22点' },
        { code: 23, name: '23点' },
      ],
      Day28Dict: [
        { code: 1, name: '1日' },
        { code: 2, name: '2日' },
        { code: 3, name: '3日' },
        { code: 4, name: '4日' },
        { code: 5, name: '5日' },
        { code: 6, name: '6日' },
        { code: 7, name: '7日' },
        { code: 8, name: '8日' },
        { code: 9, name: '9日' },
        { code: 10, name: '10日' },
        { code: 11, name: '11日' },
        { code: 12, name: '12日' },
        { code: 13, name: '13日' },
        { code: 14, name: '14日' },
        { code: 15, name: '15日' },
        { code: 16, name: '16日' },
        { code: 17, name: '17日' },
        { code: 18, name: '18日' },
        { code: 19, name: '19日' },
        { code: 20, name: '20日' },
        { code: 21, name: '21日' },
        { code: 22, name: '22日' },
        { code: 23, name: '23日' },
        { code: 24, name: '24日' },
        { code: 25, name: '25日' },
        { code: 26, name: '26日' },
        { code: 27, name: '27日' },
        { code: 28, name: '28日' }
      ]
    }
  },
  WDictDataFunc: {
    GetName: function(jsonObj, code) {
      if (code == null || code === undefined) {
        return ''
      }
      let name = ''
      jsonObj.forEach(item => {
        if (item.code === code) {
          name = item.name
        }
      })
      return name
    },
    GetObj: function(WObjData, key) {
      var obj
      WObjData.forEach(item => {
        if (item.key === key) {
          obj = item
        }
      })
      return obj
    },
    CopyObj: function(obj) {
      if (typeof obj !== 'object') {
        return obj
      }
      var newobj = {}
      for (var attr in obj) {
        newobj[attr] = this.CopyObj(obj[attr])
      }
      return newobj
    }

  },
  fromatElTime(elTime, formater = 'YYYY-MM-DD HH:mm:ss') {
    return moment(elTime).format(formater)
  }
}
