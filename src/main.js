import Vue from 'vue'
import App from './App.vue'

import moment from 'moment'// 导入文件
Vue.prototype.$moment = moment// 赋值使用
moment.locale('zh-cn')// 需要汉化

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)

import echarts from 'echarts'
Vue.prototype.$echarts = echarts

import directives from './directives'
Vue.use(directives)

import Brands from './components/Brands/index'
Vue.use(Brands)

import TimeLine from './components/TimeLine/index'
Vue.use(TimeLine)

import Fold from './components/Fold/index'
Vue.use(Fold)

import Gchart from './components/Gchart/index'
Vue.use(Gchart)

import Gradio from './components/Gradio/index'
Vue.use(Gradio)

import borderBox1 from './components/borderBox1/index'
Vue.use(borderBox1)

import createForm from './components/createForm/index'
Vue.use(createForm)

import GdropdownTree from './components/GdropdownTree/index'
Vue.use(GdropdownTree)

import GtimePoint from './components/GtimePoint/index'
Vue.use(GtimePoint)

import GtimeRange from './components/GtimeRange/index'
Vue.use(GtimeRange)

import Gtable from './components/Gtable/index'
Vue.use(Gtable)

// import Loading from './components/loading/index'
// 这时需要 use(Loading)，如果不写 Vue.use()的话，浏览器会报错，大家可以试一下
// Vue.use(Loading)

new Vue({
  el: '#app',
  render: h => h(App)
})
